#include <curl/curl.h>
#include <hiredis/hiredis.h>
#include <stdio.h>
#include <unistd.h>

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}

int main(int argc, char *argv[]) {
  uk_pr_info("Before vod worker\n");
  redisReply *qualityTask;
  redisContext *c = redisConnect("172.20.0.12", 6379);
  if (c == NULL || c->err) {
    if (c) {
      uk_pr_info("Error: %s\n", c->errstr);
      return 1;
    } else {
      uk_pr_info("Can't allocate redis context\n");
    }
  }
  uk_pr_info("Connected to Redis\n");
  sprintf(stdout, "Connected to Redis\n");
  while (1) {
    qualityTask = redisCommand(c, "BLPOP %s 0", "transcode_tasks");
    if (qualityTask == NULL || c->err) {
      fprintf(stderr, "Error reading from list\n");
      return NULL;
    }

    if (qualityTask->type != REDIS_REPLY_ARRAY || qualityTask->elements < 2) {
      fprintf(stderr, "Invalid BLPOP reply from Redis\n");
      return NULL;
    }

    if (!strcmp(qualityTask->element[1]->str, "exit")) {
      uk_pr_info("Received 'exit' message, shutting down...\n");
      return NULL;
    }
    uk_pr_info("Redis: %s => %s\n", qualityTask->element[0]->str,
               qualityTask->element[1]->str);
    char *qualityId = qualityTask->element[1]->str;
    uk_pr_info("Received videoId: %s\n", qualityId);

    redisReply *qualityPath =
        redisCommand(c, "HGET %s %s", qualityId, "transcode_id");
    if (qualityPath == NULL || c->err) {
      fprintf(stderr, "Error reading from list\n");
      return NULL;
    }

    if (qualityPath->type != REDIS_REPLY_STRING) {
      fprintf(stderr, "Invalid HGET reply from Redis\n");
      return NULL;
    }

    char *sourceId = qualityPath->str;
    char *filename;
    sprintf(filename, "/videos/%s", sourceId);

    if (access(filename, F_OK) == -1) {
      uk_pr_info("File not exists\n");
      char *qualityUrl;
      CURL *curl_handle;
      sprintf(qualityUrl, "http://172.17.0.20/qualities/%s", sourceId);
      curl_global_init(CURL_GLOBAL_ALL);
      curl_handle = curl_easy_init();
      curl_easy_setopt(curl_handle, CURLOPT_URL, qualityUrl);
      curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
      curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
      curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

      FILE *pagefile;

      pagefile = fopen(filename, "wb");
      if (pagefile) {
        uk_pr_info("Downloading %s\n", qualityUrl);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
        curl_easy_perform(curl_handle);
        fclose(pagefile);
      } else {
        uk_pr_info("Error: failed to open file\n");
      }

      curl_easy_cleanup(curl_handle);
      curl_global_cleanup();
    }
    uk_pr_info("Quality %s exists\n", sourceId);

    freeReplyObject(qualityTask);
    freeReplyObject(qualityPath);
  }

  // uk_pr_info("RESPONSE: %s -> %s\n", reply->elements[0], reply->elements[1]);

  redisFree(c);
  uk_pr_info("After vod worker\n");
}
